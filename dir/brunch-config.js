// See http://brunch.io for documentation.
exports.files = {
  javascripts: {
    joinTo: {
      'vendor.js': /^(?!app)/,
      'app.js': /^app/
    }
  },
  stylesheets: {joinTo: 'app.css'}
};

exports.plugins = {
  babel: {presets: ['latest', 'react']},
  gzip:{
    paths:{
      javascript: 'javascripts',
      stylesheet: 'stylesheets',

    },
    removeOriginalFiles: false,
    renameGzipFilesToOriginalFiles: false
  }   
};
