/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "app.css",
    "revision": "99e241a069eaf949993322705617e21a"
  },
  {
    "url": "app.css.map",
    "revision": "940e949bb34df1cc4e2f825e3967d91f"
  },
  {
    "url": "app.js",
    "revision": "a09f902b14b5d011b61df6aa8e6d7024"
  },
  {
    "url": "app.js.map",
    "revision": "958d66cce296896914de316e29877ffb"
  },
  {
    "url": "Images/ChatApp1.jpg",
    "revision": "723baafe6272c48c6e48a03e417e99c9"
  },
  {
    "url": "Images/ChatApp2.jpg",
    "revision": "c94a71983c6cde551b64c5b5451f04d2"
  },
  {
    "url": "Images/Egg1.jpg",
    "revision": "9f3d3e6e46852a477bea2b78fcc6f847"
  },
  {
    "url": "Images/Hestia1.jpg",
    "revision": "72d12e1c10b06ff38272a6ac852586fa"
  },
  {
    "url": "Images/Hestia2.jpg",
    "revision": "bd7ad134ff0150e0364cae39a9a2359a"
  },
  {
    "url": "Images/Hestia3.jpg",
    "revision": "37955a5b7498521f0cc37dfe6441581b"
  },
  {
    "url": "Images/SeamList1.jpg",
    "revision": "eadb1566379548c0ad5362d906d53c3c"
  },
  {
    "url": "Images/SeamList2.jpg",
    "revision": "14348c1517c5383ddc195ed325596b50"
  },
  {
    "url": "Images/SeamList3.jpg",
    "revision": "024e6291d59f22df4562eda521d90eeb"
  },
  {
    "url": "Images/Tether1.jpg",
    "revision": "90335cb5370fb018589b927abffabf84"
  },
  {
    "url": "Images/Tether2.jpg",
    "revision": "8115d47b3fb25e8a41b6010f5d39ed06"
  },
  {
    "url": "Images/Tether3.jpg",
    "revision": "d54d2133a2571bed2e221be7839a2c79"
  },
  {
    "url": "index.html",
    "revision": "38c129642845b7dd3caa6528a4a7d955"
  },
  {
    "url": "index.html.gz",
    "revision": "90e5bc80cef6406c4bb005915bbca3a0"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
