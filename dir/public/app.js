(function() {
  'use strict';

  var globals = typeof global === 'undefined' ? self : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var aliases = {};
  var has = {}.hasOwnProperty;

  var expRe = /^\.\.?(\/|$)/;
  var expand = function(root, name) {
    var results = [], part;
    var parts = (expRe.test(name) ? root + '/' + name : name).split('/');
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function expanded(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var hot = hmr && hmr.createHot(name);
    var module = {id: name, exports: {}, hot: hot};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var expandAlias = function(name) {
    var val = aliases[name];
    return (val && name !== val) ? expandAlias(val) : name;
  };

  var _resolve = function(name, dep) {
    return expandAlias(expand(dirname(name), dep));
  };

  var require = function(name, loaderPath) {
    if (loaderPath == null) loaderPath = '/';
    var path = expandAlias(name);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    throw new Error("Cannot find module '" + name + "' from '" + loaderPath + "'");
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  var extRe = /\.[^.\/]+$/;
  var indexRe = /\/index(\.[^\/]+)?$/;
  var addExtensions = function(bundle) {
    if (extRe.test(bundle)) {
      var alias = bundle.replace(extRe, '');
      if (!has.call(aliases, alias) || aliases[alias].replace(extRe, '') === alias + '/index') {
        aliases[alias] = bundle;
      }
    }

    if (indexRe.test(bundle)) {
      var iAlias = bundle.replace(indexRe, '');
      if (!has.call(aliases, iAlias)) {
        aliases[iAlias] = bundle;
      }
    }
  };

  require.register = require.define = function(bundle, fn) {
    if (bundle && typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          require.register(key, bundle[key]);
        }
      }
    } else {
      modules[bundle] = fn;
      delete cache[bundle];
      addExtensions(bundle);
    }
  };

  require.list = function() {
    var list = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        list.push(item);
      }
    }
    return list;
  };

  var hmr = globals._hmr && new globals._hmr(_resolve, require, modules, cache);
  require._cache = cache;
  require.hmr = hmr && hmr.wrap;
  require.brunch = true;
  globals.require = require;
})();

(function() {
var global = typeof window === 'undefined' ? this : window;
var __makeRelativeRequire = function(require, mappings, pref) {
  var none = {};
  var tryReq = function(name, pref) {
    var val;
    try {
      val = require(pref + '/node_modules/' + name);
      return val;
    } catch (e) {
      if (e.toString().indexOf('Cannot find module') === -1) {
        throw e;
      }

      if (pref.indexOf('node_modules') !== -1) {
        var s = pref.split('/');
        var i = s.lastIndexOf('node_modules');
        var newPref = s.slice(0, i).join('/');
        return tryReq(name, newPref);
      }
    }
    return none;
  };
  return function(name) {
    if (name in mappings) name = mappings[name];
    if (!name) return;
    if (name[0] !== '.' && pref) {
      var val = tryReq(name, pref);
      if (val !== none) return val;
    }
    return require(name);
  }
};
require.register("components/About/About.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = About;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function About() {
  return _react2.default.createElement(
    "div",
    { className: "about" },
    _react2.default.createElement(
      "p",
      { className: "about__text-para" },
      "Hi, I'm Gurtaj, a recent graduate from the Web Development Bootcamp at BrainStation. Before this program, I had a strong IT multidisciplinary background with focus on Quality Assurance and Technical Support(Business-facing and Customer-facing) which gives me a unique perspective as I transition into web development to use everything I have learned from post-coding stages of SDLC in an meaningful way. I have dabbled in Android Development using Java as part of the Udacity course that I completed."
    ),
    _react2.default.createElement(
      "p",
      { className: "about__text-para" },
      "My hobbies include following Arsenal, my favorite football team, playing video games across all consoles and PC(Yeap, I'm that guy who owns all platforms) and enjoying time with friends doing things I cannot currently due to Quarantine :("
    )
  );
}
});

;require.register("components/App.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require("react-router-dom");

var _Welcome = require("./Welcome/Welcome");

var _Welcome2 = _interopRequireDefault(_Welcome);

var _Portfolio = require("./Portfolio/Portfolio");

var _Portfolio2 = _interopRequireDefault(_Portfolio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
  _inherits(App, _React$Component);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).apply(this, arguments));
  }

  _createClass(App, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        _reactRouterDom.HashRouter,
        { basename: "/" },
        _react2.default.createElement(
          _reactRouterDom.Switch,
          null,
          _react2.default.createElement(_reactRouterDom.Route, { exact: true, path: "/", component: _Welcome2.default }),
          _react2.default.createElement(_reactRouterDom.Route, { path: "/portfolio", component: _Portfolio2.default })
        )
      );
    }
  }]);

  return App;
}(_react2.default.Component);

exports.default = App;
});

;require.register("components/Carousel/Carousel.jsx", function(exports, require, module) {
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSlick = require('react-slick');

var _reactSlick2 = _interopRequireDefault(_reactSlick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SimpleSlider = function (_React$Component) {
  _inherits(SimpleSlider, _React$Component);

  function SimpleSlider() {
    _classCallCheck(this, SimpleSlider);

    return _possibleConstructorReturn(this, (SimpleSlider.__proto__ || Object.getPrototypeOf(SimpleSlider)).apply(this, arguments));
  }

  _createClass(SimpleSlider, [{
    key: 'render',
    value: function render() {
      var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        draggable: false
      };
      return _react2.default.createElement(
        'div',
        { className: 'carousel' },
        _react2.default.createElement(
          _reactSlick2.default,
          settings,
          this.props.images.map(function (image, i) {
            return _react2.default.createElement(
              'div',
              { key: i },
              _react2.default.createElement('img', { src: image, className: 'carousel__image' })
            );
          })
        )
      );
    }
  }]);

  return SimpleSlider;
}(_react2.default.Component);

exports.default = SimpleSlider;
});

;require.register("components/ContactMe/ContactMe.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ContactMe;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ContactMe() {
  return _react2.default.createElement(
    "div",
    { className: "contact" },
    _react2.default.createElement(
      "p",
      { className: "contact__text" },
      "You can reach me at :"
    ),
    _react2.default.createElement(
      "div",
      { className: "contact__link-container" },
      _react2.default.createElement(
        "a",
        { href: "mailto:gurtajchhabra@gmail.com", className: "contact__links" },
        "Email"
      ),
      _react2.default.createElement(
        "a",
        {
          href: "https://www.linkedin.com/in/gurtajchhabra/",
          className: "contact__links"
        },
        "LinkedIn"
      ),
      _react2.default.createElement(
        "a",
        { href: "https://github.com/Darkthunder119", className: "contact__links" },
        "Github"
      ),
      _react2.default.createElement(
        "a",
        { href: "https://bitbucket.org/gurtajchhabra/", className: "contact__links" },
        "Bitbucket"
      )
    )
  );
}
});

;require.register("components/Portfolio/Portfolio.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _About = require("../About/About");

var _About2 = _interopRequireDefault(_About);

var _ContactMe = require("../ContactMe/ContactMe");

var _ContactMe2 = _interopRequireDefault(_ContactMe);

var _Projects = require("../Projects/Projects");

var _Projects2 = _interopRequireDefault(_Projects);

var _TechStack = require("../TechStack/TechStack");

var _TechStack2 = _interopRequireDefault(_TechStack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Portfolio = function (_React$Component) {
  _inherits(Portfolio, _React$Component);

  function Portfolio() {
    _classCallCheck(this, Portfolio);

    var _this = _possibleConstructorReturn(this, (Portfolio.__proto__ || Object.getPrototypeOf(Portfolio)).call(this));

    _this.state = {
      current: ''
    };
    _this.clickHander = _this.clickHander.bind(_this);
    return _this;
  }

  _createClass(Portfolio, [{
    key: "clickHander",
    value: function clickHander(event) {
      var textVal = event.target.textContent.split('');
      textVal.shift();
      textVal.pop();
      var finalVal = textVal.join('');
      this.setState({ current: finalVal });
    }
  }, {
    key: "renderSwitch",
    value: function renderSwitch() {
      switch (this.state.current) {
        case 'About Me':
          return _react2.default.createElement(_About2.default, null);
        case 'Projects':
          return _react2.default.createElement(_Projects2.default, null);
        case 'Contact Me':
          return _react2.default.createElement(_ContactMe2.default, null);
        case 'Tech Stack':
          return _react2.default.createElement(_TechStack2.default, null);
        default:
          return null;
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "section",
        { className: "portfolio" },
        _react2.default.createElement(
          "h1",
          { className: "portfolio__header portfolio__animated" },
          "ROBCO Industries (TM) Fetching Profiles"
        ),
        _react2.default.createElement(
          "h2",
          { className: "portfolio__header-two" },
          "Clearance: Full Stack Developer"
        ),
        _react2.default.createElement(
          "h3",
          { className: "portfolio__header-three" },
          "Select a category:"
        ),
        _react2.default.createElement(
          "ul",
          { className: "portfolio__list" },
          _react2.default.createElement(
            "li",
            { className: "portfolio__links", onClick: this.clickHander },
            "[About Me]"
          ),
          _react2.default.createElement(
            "li",
            { className: "portfolio__links", onClick: this.clickHander },
            "[Projects]"
          ),
          _react2.default.createElement(
            "li",
            { className: "portfolio__links", onClick: this.clickHander },
            "[Tech Stack]"
          ),
          _react2.default.createElement(
            "li",
            { className: "portfolio__links", onClick: this.clickHander },
            "[Contact Me]"
          )
        ),
        _react2.default.createElement(
          "div",
          { className: "portfolio__div" },
          "Select an option: ",
          this.state.current ? _react2.default.createElement(
            "div",
            { className: "portfolio__current" },
            this.state.current
          ) : null,
          _react2.default.createElement("p", { className: "square" })
        ),
        this.renderSwitch()
      );
    }
  }]);

  return Portfolio;
}(_react2.default.Component);

exports.default = Portfolio;
});

;require.register("components/Project/Project.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Project;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _Carousel = require("../Carousel/Carousel");

var _Carousel2 = _interopRequireDefault(_Carousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Project(props) {
  return _react2.default.createElement(
    "div",
    { className: "project" },
    _react2.default.createElement(
      "p",
      { className: "project__text" },
      props.data.name
    ),
    _react2.default.createElement(
      "p",
      { className: "project__text-two" },
      props.data.description
    ),
    _react2.default.createElement(
      "div",
      { className: "project__link-container" },
      props.data.link && _react2.default.createElement(
        "a",
        { href: props.data.link, target: "_blank", className: "project__links" },
        "Link"
      ),
      props.data.demo && _react2.default.createElement(
        "a",
        { href: props.data.demo, target: "_blank", className: "project__links" },
        "Demo"
      )
    ),
    _react2.default.createElement(_Carousel2.default, { images: props.data.images })
  );
}
});

;require.register("components/Projects/Projects.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = Projects;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactSpring = require("react-spring");

var _reactUseGesture = require("react-use-gesture");

var _Project = require("../Project/Project");

var _Project2 = _interopRequireDefault(_Project);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var cards = [{
  name: "Eggplants and Cucumbers",
  description: "Pair programming project created in 6 hrs using React at BrainStation",
  images: ["/Images/Egg1.jpg"],
  link: "https://eggplantsandcucumbers.surge.sh"
}, {
  name: "Bstn3D Hackathon-SeamList",
  description: "BrainStation Hackathon entry working with Data Scientists and UX designers to deliver a food blog in 6 hrs",
  images: ["/Images/SeamList1.jpg", "/Images/SeamList2.jpg", "/Images/SeamList3.jpg"],
  link: "https://bstn-3d-winter-2020-team-4.netlify.app/"
}, {
  name: "ChatApp",
  description: "A Chat App created using Firebase and React as a collab project",
  images: ["/Images/ChatApp1.jpg", "/Images/ChatApp2.jpg"],
  link: "https://chat-app-22717.web.app/"
}, {
  name: "Microsoft Hackathon-Tether",
  description: "Microsoft Hackathon entry for COVID-19 WFH ideas. An app that combines Slack, Jira and Pomodoro techniques via integrations and is built using React",
  images: ["/Images/Tether1.jpg", "/Images/Tether2.jpg", "/Images/Tether3.jpg"],
  link: "https://tethermsft.web.app"
}, {
  name: "Hestia",
  description: "Capstone project for BrainStation using React, MapBox, DeckGL and D3js for rendering Toronto Crime Statistics",
  images: ["/Images/Hestia1.jpg", "/Images/Hestia2.jpg", "/Images/Hestia3.jpg"],
  link: "https://hestiaapp.netlify.app",
  demo: "https://www.loom.com/share/0e48c0659e814e7b99c15f0cd1e456cc"
}];
// These two are just helpers, they curate spring data, values that are later being interpolated into css
var to = function to(i) {
  return {
    x: 0,
    y: i * -4,
    scale: 1,
    rot: -10 + Math.random() * 20,
    delay: i * 100
  };
};
var from = function from(i) {
  return { x: 0, rot: 0, scale: 1.5, y: -1000 };
};
// This is being used down there in the view, it interpolates rotation and scale into a css transform
var trans = function trans(r, s) {
  return "perspective(1500px) rotateX(30deg) rotateY(" + r / 10 + "deg) rotateZ(" + r + "deg) scale(" + s + ")";
};

function Projects() {
  var _useState = (0, _react.useState)(function () {
    return new Set();
  }),
      _useState2 = _slicedToArray(_useState, 1),
      gone = _useState2[0]; // The set flags all the cards that are flicked out


  var _useSprings = (0, _reactSpring.useSprings)(cards.length, function (i) {
    return {
      to: to(i),
      from: from(i)
    };
  }),
      _useSprings2 = _slicedToArray(_useSprings, 2),
      props = _useSprings2[0],
      set = _useSprings2[1]; // Create a bunch of springs using the helpers above
  // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity


  var bind = (0, _reactUseGesture.useGesture)(function (_ref) {
    var _ref$args = _slicedToArray(_ref.args, 1),
        index = _ref$args[0],
        down = _ref.down,
        _ref$delta = _slicedToArray(_ref.delta, 1),
        xDelta = _ref$delta[0],
        distance = _ref.distance,
        _ref$direction = _slicedToArray(_ref.direction, 1),
        xDir = _ref$direction[0],
        velocity = _ref.velocity;

    var trigger = velocity > 0.2; // If you flick hard enough it should trigger the card to fly out
    var dir = xDir < 0 ? -1 : 1; // Direction should either point left or right
    if (!down && trigger) gone.add(index); // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
    set(function (i) {
      if (index !== i) return; // We're only interested in changing spring-data for the current spring
      var isGone = gone.has(index);
      var x = isGone ? (200 + window.innerWidth) * dir : down ? xDelta : 0; // When a card is gone it flys out left or right, otherwise goes back to zero
      var rot = xDelta / 100 + (isGone ? dir * 10 * velocity : 0); // How much the card tilts, flicking it harder makes it rotate faster
      var scale = down ? 1.15 : 1; // Active cards lift up a bit
      return {
        x: x,
        rot: rot,
        scale: scale,
        delay: undefined,
        config: { friction: 50, tension: down ? 800 : isGone ? 200 : 500 }
      };
    });
    if (!down && gone.size === cards.length) setTimeout(function () {
      return gone.clear() || set(function (i) {
        return to(i);
      });
    }, 600);
  });
  // Now we're just mapping the animated values to our view, that's it. Btw, this component only renders once. :-)
  return _react2.default.createElement(
    "div",
    { className: "projects" },
    props.map(function (_ref2, i) {
      var x = _ref2.x,
          y = _ref2.y,
          rot = _ref2.rot,
          scale = _ref2.scale;
      return _react2.default.createElement(
        _reactSpring.animated.div,
        {
          key: i,
          style: {
            transform: (0, _reactSpring.interpolate)([x, y], function (x, y) {
              return "translate3d(" + x + "px," + y + "px,0)";
            })
          },
          className: "projects__outer"
        },
        _react2.default.createElement(
          _reactSpring.animated.div,
          _extends({}, bind(i), {
            style: { transform: (0, _reactSpring.interpolate)([rot, scale], trans) },
            className: "projects__inner"
          }),
          _react2.default.createElement(_Project2.default, { data: cards[i] })
        )
      );
    })
  );
}
});

;require.register("components/TechStack/TechStack.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = TechStack;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _freeBrandsSvgIcons = require("@fortawesome/free-brands-svg-icons");

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _reactFontawesome = require("@fortawesome/react-fontawesome");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TechStack() {
  return _react2.default.createElement(
    "div",
    { className: "tech-stack" },
    _react2.default.createElement(
      "ul",
      { className: "tech-stack__link-container" },
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faReact }),
        " React"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faJs }),
        " JavaScript"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faHtml5 }),
        " HTML5"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faCss3Alt }),
        " CSS"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faSass }),
        " Sass"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faNode }),
        " Node"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeSolidSvgIcons.faDatabase }),
        " Realtime Firebase, MongoDB"
      ),
      _react2.default.createElement(
        "li",
        { className: "tech-stack__links" },
        _react2.default.createElement(_reactFontawesome.FontAwesomeIcon, { icon: _freeBrandsSvgIcons.faGit }),
        " Git"
      )
    )
  );
}
});

;require.register("components/Welcome/Welcome.jsx", function(exports, require, module) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Welcome = function (_React$Component) {
  _inherits(Welcome, _React$Component);

  function Welcome(props) {
    _classCallCheck(this, Welcome);

    var _this = _possibleConstructorReturn(this, (Welcome.__proto__ || Object.getPrototypeOf(Welcome)).call(this, props));

    _this.clickArray = [];
    _this.finalArray = ["G", "U", "R", "T", "A", "J"];
    _this.letterClickHandler = _this.letterClickHandler.bind(_this);
    return _this;
  }

  _createClass(Welcome, [{
    key: "letterClickHandler",
    value: function letterClickHandler(e) {
      var _this2 = this;

      if (this.finalArray.includes(e.target.innerText)) {
        this.clickArray.push(e.target.innerText);
        e.target.classList.add("welcome__found-clicked");
      }
      if (this.finalArray.every(function (val) {
        return _this2.clickArray.includes(val);
      })) {
        this.props.history.push("/portfolio");
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "section",
        { className: "welcome" },
        _react2.default.createElement(
          "div",
          { className: "welcome__header" },
          "Click/Touch on the distinct letters to proceed (Hint: check colors)"
        ),
        _react2.default.createElement(
          "div",
          { className: "welcome__text", onClick: this.letterClickHandler },
          _react2.default.createElement(
            "span",
            null,
            "...----...."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "..-:\"''         ''\"-.."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".-'                      '-."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".'              .     .       '."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".'   .          .    .      .    .''."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".'  .    .       .   .   .     .   . ..:."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".' .   . .  .       .   .   ..  .   . ....::."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "..   .   .      .  .    .     .  ..  . ....:IA."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:  .   .    .    .  .  .    .. .  .. .. ....:IA."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".: .   .   ..   .    .     . . .. . ... ....:.:VHA."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "'..  .  .. .   .       .  . .. . .. . .....:.::IHHB."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:. .  . .  . .   .  .  . . . ...:.:... .......:HIHMM."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:.... .   . .\"::\"'.. .   .  . .:.:.:II;,. .. ..:IHIMMA"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "':.:..  ..::IHHHHHI::. . .  ...:.::::.,,,. . ....VIMMHM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:::I. .AHHHHHHHHHHAI::. .:...,:IIHHHHHHMMMHHL:. . VMMMM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:.:V.:IVHHHHHHHMHMHHH::..:\" .:HIHHHHHHHHHHHHHMHHA. .VMMM."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":..V.:IVHHHHHMMHHHHHHHB... . .:VPHHMHHH",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "HHHHHAI.:VMMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "::V..:VIHHH",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "HHHHHH. .   .I\":IIMHHMMHHHHHHHHHHHAPI:WMM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "::\". .:.HHHHHHHHMMHHHHHI.  . .:..I:MHMMHHHHHHHHHMHV:':H:WM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":: . :.::IIHHHHHHMMHHHHV  .ABA.:.:IMHMHMMMHMHHHHV:'. .IHWW"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "'.  ..:..:.:IHHHHHMMHV\" .AVMHMA.:.'VHMMMGHHHHHV:' .  :IHWV"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":.  .:...:\".:.:TPP\"   .AVMMHMMA.:. \"VMMHHHP.:... .. :IVAI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".:.   '... .:\"'   .   ..HMMMHMMMA::. .\"VHHI:::....  .:IHW'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "...  .  . ..:IIPPIH: ..HMMMI.MMMV:I:.  .:ILLH:.. ...:I:IM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ": .   .'\"' .:.V\". .. .  :HMMM:IMMMI::I. ..:HHIIPPHI::'.P:HM."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":.  .  .  .. ..:.. .    :AMMM IMMMM..:...:IV\":T::I::.\".:IHIMA"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "'V:.. .. . .. .  .  .   'VMMV..VMMV :....:V:.:..:....::IHHHMH"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "\"IHH:.II:.. .:. .  . . . \" :HB\"\" . . ..PI:.::.:::..:IHHMMV\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":IP\"\"HHII:.  .  .    . . .'V:. . . ..:IH:.:.::IHIHHMMMMM\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":V:. VIMA:I..  .     .  . .. . .  .:.I:I:..:IHHHHMMHHMMM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":\"VI:.VWMA::. .:      .   .. .:. ..:.I::.:IVHHHMMHHMMMMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":.\"VIIHHMMA:.  .   .   .:  .:.. . .:.II:I:AMMMMMMHMMMMMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":..VIHIHMMMI...::.,:.,:!\"I:!\"I!\"I!\"V:AI:VAMMMMM",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "MM'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "':.:HIHIMHHA:\"!!\"I.:AXXXVVVXXXXXXA:.\"HPHIMMMMHHMHMMMMMV"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "V:H:I:MA:W'I :AXXXIXII:IIIISSSSSSXXA.I.VMMMHMHMMMMMM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "'I::IVA ASSSS",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "BSBMBSSSSSSBBMMMBS.VVMMHIMM'\"'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "I:: VPAIMSSSSSSSSSBSSSMMSSSSBBMMMMXXI:MMHIMMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ".I::. \"H:XIIXBBMMMM",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "MMGMMMMBXIXXMMPHIIMM'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":::I.  ':XSSXXIIIIXSSBMBSSXXXIIIXXSMMAMI:.IMM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":::I:.  .VSSSSSSSISISSSBII:ISSSSBMMB:MI:..:MM"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "::.I:.  ':\"SSSSSSSISISSXIIXSSSSBMMB:AHI:..MMM."
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "::.I:. . ..:\"BBSSSS",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "SSBBBMMMB:AHHI::.HMMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            ":..::.  . ..::\":BBBBSSSBBBMMMB:MMMMHHII::IHHMI"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "':.I:... ....:IHHHHHMMMMMMMMMMMMMMMHHIIIIHMMV\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "\"V:. ..:...:.IHHHMMMMMMMMMMMMMMMMHHHMHHMHP'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "':. .:::.:.::III::IHHHHMMMMMHMHMMHHHHM\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "\"::....::.:::..:..::IIIIIHHHHMMMHHMV\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "\"::.::.. .. .  ...:::IIHHMMMMHMV\""
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "\"V::... . .I::IHHMMV\"'"
          ),
          _react2.default.createElement("br", null),
          _react2.default.createElement(
            "span",
            null,
            "'\"VHVH",
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "G"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "U"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "R"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "T"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "A"
            ),
            _react2.default.createElement(
              "span",
              { className: "welcome__found" },
              "J"
            ),
            "HMMV:\"'"
          ),
          _react2.default.createElement("br", null)
        )
      );
    }
  }]);

  return Welcome;
}(_react2.default.Component);

exports.default = Welcome;
});

;require.register("initialize.js", function(exports, require, module) {
'use strict';

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _App = require('components/App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.addEventListener('DOMContentLoaded', function () {
  _reactDom2.default.render(_react2.default.createElement(_App2.default, null), document.querySelector('#app'));
});
});

require.register("___globals___", function(exports, require, module) {
  
});})();require('___globals___');


//# sourceMappingURL=app.js.map