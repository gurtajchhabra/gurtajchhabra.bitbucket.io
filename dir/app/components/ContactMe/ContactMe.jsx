import React from "react";

export default function ContactMe() {
  return (
    <div className="contact">
      <p className="contact__text">You can reach me at :</p>
      <div className="contact__link-container">
        <a href="mailto:gurtajchhabra@gmail.com" className="contact__links">
          Email
        </a>
        <a
          href="https://www.linkedin.com/in/gurtajchhabra/"
          className="contact__links"
        >
          LinkedIn
        </a>
        <a href="https://github.com/Darkthunder119" className="contact__links">
          Github
        </a>
        <a href="https://bitbucket.org/gurtajchhabra/" className="contact__links">
          Bitbucket
        </a>
      </div>
    </div>
  );
}
