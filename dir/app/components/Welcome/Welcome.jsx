import React from "react";

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.clickArray = [];
    this.finalArray = ["G","U","R","T","A","J"];
    this.letterClickHandler = this.letterClickHandler.bind(this);
  }
  letterClickHandler(e){
    if(this.finalArray.includes(e.target.innerText)){
      this.clickArray.push(e.target.innerText);
      e.target.classList.add("welcome__found-clicked");
    }
    if (this.finalArray.every((val) => this.clickArray.includes(val))) {
      this.props.history.push("/portfolio");
    }
  }
  render() {
    return (
      <section className="welcome">
        <div className="welcome__header">Click/Touch on the distinct letters to proceed (Hint: check colors)</div>
        <div className="welcome__text" onClick={this.letterClickHandler}>
            <span>...----....</span>
            <br></br>
            <span>..-:"''         ''"-..</span>
            <br></br>
            <span>.-'                      '-.</span>
            <br></br>
            <span>.'              .     .       '.</span>
            <br></br>
            <span>.'   .          .    .      .    .''.</span>
            <br></br>
            <span>.'  .    .       .   .   .     .   . ..:.</span>
            <br></br>
            <span>.' .   . .  .       .   .   ..  .   . ....::.</span>
            <br></br>
            <span>..   .   .      .  .    .     .  ..  . ....:IA.</span>
            <br></br>
            <span>.:  .   .    .    .  .  .    .. .  .. .. ....:IA.</span>
            <br></br>
            <span>.: .   .   ..   .    .     . . .. . ... ....:.:VHA.</span>
            <br></br>
            <span>'..  .  .. .   .       .  . .. . .. . .....:.::IHHB.</span>
            <br></br>
            <span>.:. .  . .  . .   .  .  . . . ...:.:... .......:HIHMM.</span>
            <br></br>
            <span>.:.... .   . ."::"'.. .   .  . .:.:.:II;,. .. ..:IHIMMA</span>
            <br></br>
            <span>':.:..  ..::IHHHHHI::. . .  ...:.::::.,,,. . ....VIMMHM</span>
            <br></br>
            <span>.:::I. .AHHHHHHHHHHAI::. .:...,:IIHHHHHHMMMHHL:. . VMMMM</span>
            <br></br>
            <span>.:.:V.:IVHHHHHHHMHMHHH::..:" .:HIHHHHHHHHHHHHHMHHA. .VMMM.</span>
            <br></br>
            <span>:..V.:IVHHHHHMMHHHHHHHB... . .:VPHHMHHH<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>HHHHHAI.:VMMI</span>
            <br></br>
            <span>::V..:VIHHH<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>HHHHHH. .   .I":IIMHHMMHHHHHHHHHHHAPI:WMM</span>
            <br></br>
            <span>::". .:.HHHHHHHHMMHHHHHI.  . .:..I:MHMMHHHHHHHHHMHV:':H:WM</span>
            <br></br>
            <span>:: . :.::IIHHHHHHMMHHHHV  .ABA.:.:IMHMHMMMHMHHHHV:'. .IHWW</span>
            <br></br>
            <span>'.  ..:..:.:IHHHHHMMHV" .AVMHMA.:.'VHMMMGHHHHHV:' .  :IHWV</span>
            <br></br>
            <span>:.  .:...:".:.:TPP"   .AVMMHMMA.:. "VMMHHHP.:... .. :IVAI</span>
            <br></br>
            <span>.:.   '... .:"'   .   ..HMMMHMMMA::. ."VHHI:::....  .:IHW'</span>
            <br></br>
            <span>...  .  . ..:IIPPIH: ..HMMMI.MMMV:I:.  .:ILLH:.. ...:I:IM</span>
            <br></br>
            <span>: .   .'"' .:.V". .. .  :HMMM:IMMMI::I. ..:HHIIPPHI::'.P:HM.</span>
            <br></br>
            <span>:.  .  .  .. ..:.. .    :AMMM IMMMM..:...:IV":T::I::.".:IHIMA</span>
            <br></br>
            <span>'V:.. .. . .. .  .  .   'VMMV..VMMV :....:V:.:..:....::IHHHMH</span>
            <br></br>
            <span>"IHH:.II:.. .:. .  . . . " :HB"" . . ..PI:.::.:::..:IHHMMV"</span>
            <br></br>
            <span>:IP""HHII:.  .  .    . . .'V:. . . ..:IH:.:.::IHIHHMMMMM"</span>
            <br></br>
            <span>:V:. VIMA:I..  .     .  . .. . .  .:.I:I:..:IHHHHMMHHMMM</span>
            <br></br>
            <span>:"VI:.VWMA::. .:      .   .. .:. ..:.I::.:IVHHHMMHHMMMMI</span>
            <br></br>
            <span>:."VIIHHMMA:.  .   .   .:  .:.. . .:.II:I:AMMMMMMHMMMMMI</span>
            <br></br>
            <span>:..VIHIHMMMI...::.,:.,:!"I:!"I!"I!"V:AI:VAMMMMM<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>MM'</span>
            <br></br>
            <span>':.:HIHIMHHA:"!!"I.:AXXXVVVXXXXXXA:."HPHIMMMMHHMHMMMMMV</span>
            <br></br>
            <span>V:H:I:MA:W'I :AXXXIXII:IIIISSSSSSXXA.I.VMMMHMHMMMMMM</span>
            <br></br>
            <span>'I::IVA ASSSS<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>BSBMBSSSSSSBBMMMBS.VVMMHIMM'"'</span>
            <br/>
            <span>I:: VPAIMSSSSSSSSSBSSSMMSSSSBBMMMMXXI:MMHIMMI</span>
            <br/>
            <span>.I::. "H:XIIXBBMMMM<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>MMGMMMMBXIXXMMPHIIMM'</span>
            <br/>
            <span>:::I.  ':XSSXXIIIIXSSBMBSSXXXIIIXXSMMAMI:.IMM</span>
            <br/>
            <span>:::I:.  .VSSSSSSSISISSSBII:ISSSSBMMB:MI:..:MM</span>
            <br/>
            <span>::.I:.  ':"SSSSSSSISISSXIIXSSSSBMMB:AHI:..MMM.</span>
            <br/>
            <span>::.I:. . ..:"BBSSSS<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>SSBBBMMMB:AHHI::.HMMI</span>
            <br/>
            <span>:..::.  . ..::":BBBBSSSBBBMMMB:MMMMHHII::IHHMI</span>
            <br/>
            <span>':.I:... ....:IHHHHHMMMMMMMMMMMMMMMHHIIIIHMMV"</span>
            <br/>
            <span>"V:. ..:...:.IHHHMMMMMMMMMMMMMMMMHHHMHHMHP'</span>
            <br/>
            <span>':. .:::.:.::III::IHHHHMMMMMHMHMMHHHHM"</span>
            <br/>
            <span>"::....::.:::..:..::IIIIIHHHHMMMHHMV"</span>
            <br/>
            <span>"::.::.. .. .  ...:::IIHHMMMMHMV"</span>
            <br/>
            <span>"V::... . .I::IHHMMV"'</span>
            <br/>
            <span>'"VHVH<span className="welcome__found">G</span><span className="welcome__found">U</span><span className="welcome__found">R</span><span className="welcome__found">T</span><span className="welcome__found">A</span><span className="welcome__found">J</span>HMMV:"'</span>
            <br/>
        </div>                    
      </section>
    );
  }
}
