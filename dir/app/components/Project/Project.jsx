import React from "react";
import Carousel from "../Carousel/Carousel";

export default function Project(props) {
  return (
    <div className="project">
      <p className="project__text">{props.data.name}</p>
      <p className="project__text-two">{props.data.description}</p>
      <div className="project__link-container">
        {props.data.link && <a href={props.data.link} target="_blank" className="project__links">Link</a>}
        {props.data.demo && <a href={props.data.demo} target="_blank" className="project__links">Demo</a>}
      </div>
      <Carousel images={props.data.images} />
    </div>
  );
}
