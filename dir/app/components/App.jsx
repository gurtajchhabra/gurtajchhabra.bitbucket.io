import React from "react";
import { HashRouter, Route, Switch, Link } from "react-router-dom";
import Welcome from "./Welcome/Welcome";
import Portfolio from "./Portfolio/Portfolio";

export default class App extends React.Component {
  render() {
    return (
      <HashRouter basename="/">
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route path="/portfolio" component={Portfolio}/>
        </Switch>
      </HashRouter>
    );
  }
}
