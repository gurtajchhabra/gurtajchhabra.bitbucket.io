import React from "react";
import About from '../About/About'
import ContactMe from "../ContactMe/ContactMe";
import Projects from "../Projects/Projects";
import TechStack from "../TechStack/TechStack";
export default class Portfolio extends React.Component {
  constructor() {
    super();
    this.state={
        current: ''
    }
    this.clickHander = this.clickHander.bind(this);
  }
  
  clickHander(event) {
    let textVal =event.target.textContent.split('');
    textVal.shift();
    textVal.pop();
    let finalVal = textVal.join('');
    this.setState({current: finalVal})
  }
  renderSwitch(){
    switch(this.state.current){
      case 'About Me' :
        return <About />
      case 'Projects' :
        return <Projects />
      case 'Contact Me' :
        return <ContactMe />
      case 'Tech Stack' :
        return <TechStack />
      default:
        return null
    }
  }
  render() {
    return (
      <section className="portfolio">
        <h1 className="portfolio__header portfolio__animated">
          ROBCO Industries (TM) Fetching Profiles
        </h1>
        <h2 className="portfolio__header-two">
          Clearance: Full Stack Developer
        </h2>
        <h3 className="portfolio__header-three">Select a category:</h3>
        <ul className="portfolio__list">
          <li className="portfolio__links" onClick={this.clickHander}>
            [About Me]
          </li>
          <li className="portfolio__links" onClick={this.clickHander}>
            [Projects]
          </li>
          <li className="portfolio__links" onClick={this.clickHander}>
            [Tech Stack]
          </li>
          <li className="portfolio__links" onClick={this.clickHander}>
            [Contact Me]
          </li>
        </ul>
        <div className="portfolio__div">Select an option: {this.state.current ? <div className="portfolio__current">{this.state.current}</div>: null}
        <p className="square"></p>
        </div>  
        {this.renderSwitch()}
      </section>
    );
  }
}
