import React from "react";

export default function About() {
  return (
    <div className="about">
      <p className="about__text-para">
        Hi, I'm Gurtaj, a recent graduate from the Web Development Bootcamp at
        BrainStation. Before this program, I had a strong IT multidisciplinary
        background with focus on Quality Assurance and Technical
        Support(Business-facing and Customer-facing) which gives me a unique
        perspective as I transition into web development to use everything I
        have learned from post-coding stages of SDLC in an meaningful way. I have
        dabbled in Android Development using Java as part of the Udacity course
        that I completed.
      </p>
      <p className="about__text-para">
        My hobbies include following Arsenal, my favorite football team, playing
        video games across all consoles and PC(Yeap, I'm that guy who owns all
        platforms) and enjoying time with friends doing things I cannot
        currently due to Quarantine :(
      </p>
    </div>
  );
}
