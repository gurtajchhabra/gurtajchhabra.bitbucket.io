import React, { useState } from "react";
import { useSprings, animated, interpolate } from "react-spring";
import { useGesture } from "react-use-gesture";
import Project from '../Project/Project';

let cards = [
  {
    name: "Eggplants and Cucumbers",
    description:
      "Pair programming project created in 6 hrs using React at BrainStation",
    images: [
      "/Images/Egg1.jpg"
    ],
    link: "https://eggplantsandcucumbers.surge.sh"
  },
  {
    name: "Bstn3D Hackathon-SeamList",
    description:
      "BrainStation Hackathon entry working with Data Scientists and UX designers to deliver a food blog in 6 hrs",
    images: [
      "/Images/SeamList1.jpg",
      "/Images/SeamList2.jpg",
      "/Images/SeamList3.jpg",
    ],
    link: "https://bstn-3d-winter-2020-team-4.netlify.app/"
  },  
  {
    name: "ChatApp",
    description:
      `A Chat App created using Firebase and React as a collab project`,
    images: [
      "/Images/ChatApp1.jpg",
      "/Images/ChatApp2.jpg"
    ],
    link:"https://chat-app-22717.web.app/"
  },

  {
    name: "Microsoft Hackathon-Tether",
    description:
      "Microsoft Hackathon entry for COVID-19 WFH ideas. An app that combines Slack, Jira and Pomodoro techniques via integrations and is built using React",
    images: [
      "/Images/Tether1.jpg",
      "/Images/Tether2.jpg",
      "/Images/Tether3.jpg",
    ],
    link: "https://tethermsft.web.app"
  },
  {
    name: "Hestia",
    description:
      "Capstone project for BrainStation using React, MapBox, DeckGL and D3js for rendering Toronto Crime Statistics",
    images: [
      "/Images/Hestia1.jpg",
      "/Images/Hestia2.jpg",
      "/Images/Hestia3.jpg",
    ],
    link: "https://hestiaapp.netlify.app",
    demo: "https://www.loom.com/share/0e48c0659e814e7b99c15f0cd1e456cc"
  },
];
// These two are just helpers, they curate spring data, values that are later being interpolated into css
const to = (i) => ({
  x: 0,
  y: i * -4,
  scale: 1,
  rot: -10 + Math.random() * 20,
  delay: i * 100,
});
const from = (i) => ({ x: 0, rot: 0, scale: 1.5, y: -1000 });
// This is being used down there in the view, it interpolates rotation and scale into a css transform
const trans = (r, s) =>
  `perspective(1500px) rotateX(30deg) rotateY(${
    r / 10
  }deg) rotateZ(${r}deg) scale(${s})`;

export default function Projects() {
  const [gone] = useState(() => new Set()); // The set flags all the cards that are flicked out
  const [props, set] = useSprings(cards.length, (i) => ({
    to: to(i),
    from: from(i),
  })); // Create a bunch of springs using the helpers above
  // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity
  const bind = useGesture(
    ({
      args: [index],
      down,
      delta: [xDelta],
      distance,
      direction: [xDir],
      velocity,
    }) => {
      const trigger = velocity > 0.2; // If you flick hard enough it should trigger the card to fly out
      const dir = xDir < 0 ? -1 : 1; // Direction should either point left or right
      if (!down && trigger) gone.add(index); // If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
      set((i) => {
        if (index !== i) return; // We're only interested in changing spring-data for the current spring
        const isGone = gone.has(index);
        const x = isGone ? (200 + window.innerWidth) * dir : down ? xDelta : 0; // When a card is gone it flys out left or right, otherwise goes back to zero
        const rot = xDelta / 100 + (isGone ? dir * 10 * velocity : 0); // How much the card tilts, flicking it harder makes it rotate faster
        const scale = down ? 1.15 : 1; // Active cards lift up a bit
        return {
          x,
          rot,
          scale,
          delay: undefined,
          config: { friction: 50, tension: down ? 800 : isGone ? 200 : 500 },
        };
      });
      if (!down && gone.size === cards.length)
        setTimeout(() => gone.clear() || set((i) => to(i)), 600);
    }
  );
  // Now we're just mapping the animated values to our view, that's it. Btw, this component only renders once. :-)
  return (
    <div className="projects">
      {props.map(({ x, y, rot, scale }, i) => (
        <animated.div
          key={i}
          style={{
            transform: interpolate(
              [x, y],
              (x, y) => `translate3d(${x}px,${y}px,0)`
            ),
          }}
          className="projects__outer"
        >
          {/* This is the card itself, we're binding our gesture to it (and inject its index so we know which is which) */}
          <animated.div
            {...bind(i)}
            style={{ transform: interpolate([rot, scale], trans) }}
            className="projects__inner"
          >
            <Project data={cards[i]}/>
          </animated.div>
        </animated.div>
      ))}
    </div>
  );
}
