import React from 'react';
import Slider from 'react-slick';

export default class SimpleSlider extends React.Component {
    render() {
      const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        draggable: false
      };
      return (
        <div className="carousel">
          <Slider {...settings}>
            {this.props.images.map((image,i)=><div key={i}><img src={image} className="carousel__image"/></div>)}
          </Slider>
        </div>
      );
    }
  }