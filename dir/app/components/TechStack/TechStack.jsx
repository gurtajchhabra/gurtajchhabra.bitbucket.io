import React from "react";
import {
  faReact,
  faHtml5,
  faSass,
  faCss3Alt,
  faJs,
  faNode,
  faGit,
} from "@fortawesome/free-brands-svg-icons";
import { faDatabase } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function TechStack() {
  return (
    <div className="tech-stack">
      <ul className="tech-stack__link-container">
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faReact} /> React
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faJs} /> JavaScript
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faHtml5} /> HTML5
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faCss3Alt} /> CSS
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faSass} /> Sass
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faNode} /> Node
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faDatabase} /> Realtime Firebase, MongoDB
        </li>
        <li className="tech-stack__links">
          <FontAwesomeIcon icon={faGit} /> Git
        </li>
      </ul>
    </div>
  );
}
